/*
 * font.h
 *
 *  Created on: Feb 19, 2019
 *      Author: chungnguyen
 */

#ifndef P10_FONT_H_
#define P10_FONT_H_
#include "stm32f3xx_hal.h"
#include <stdbool.h>

typedef struct
{
	bool code[5][4];
} font54_t;

typedef struct
{
	bool code[7][6];
} font76_t;
#endif /* P10_FONT_H_ */
