/*
 * P10.h
 *
 *  Created on: Jan 24, 2019
 *      Author: chungnguyen
 */

#ifndef P10_P10_H_
#define P10_P10_H_
#include "stm32f3xx_hal.h"
#include <stdbool.h>
#define MAX_ROW_SCAN 8
#define MAX_ROW		 32
#define MAX_COL		 32

typedef struct
{
	bool r;
	bool g;
	bool b;
} pixel_t;

typedef enum
{
	NoColor = 0,
	Red,
	Green,
	Blue,
	Yellow,
	Cyan,
	Magenta,
	White,
	MAX_COLOR
} enumColor_t;

typedef struct
{
	uint8_t x:5;
	uint8_t y:5;
} point_2D_t;


typedef enum pinNumberP10
{
	R1_PIN = 0,
	G1_PIN,
	B1_PIN,
	R2_PIN,
	G2_PIN,
	B2_PIN,
	A_PIN,
	B_PIN,
	C_PIN,
	CLK_PIN,
	LAT_PIN,
	OE_PIN,
	MAX_NUM_PIN_P10
} pinNumber_t;

typedef enum
{
	FONT54 = 0,
	FONT76
} formatFont_t;

typedef enum
{
	SCROLL_DISABLE,
	SCROLL_UP,
	SCROLL_DOWN,
	SCROLL_LEFT,
	SCROLL_RIGHT
} scrollType_t;
void P10_setColorType1At(uint8_t row, uint8_t col, enumColor_t color);
void P10_setColorType2At(uint8_t row, uint8_t col, bool r, bool g, bool b);
void P10_clearScreen(void);
void P10_clearArea(uint8_t row_upperLeftCorner, uint8_t col_upperLeftCorner, uint8_t row_underRightCorner, uint8_t col_underRightCorner);
void P10_processBrightness(void);
void P10_setBrightness(uint8_t brn);
void P10_sendString(uint8_t row, uint8_t col, uint8_t *str, formatFont_t fontFormat, enumColor_t color);
void P10_sendNumber(uint8_t row, uint8_t col, uint8_t number, formatFont_t fontFormat, enumColor_t color);
void P10_drawframe(uint8_t row_upperLeftCorner, uint8_t col_upperLeftCorner, uint8_t row_underRightCorner, uint8_t col_underRightCorner, enumColor_t color);
void P10_scroll(uint8_t *str, scrollType_t direct);
void P10_scrollProcess(void);
enumColor_t P10_getColorAt(uint8_t row, uint8_t col);
#endif /* P10_P10_H_ */
