/*
 * snake.h
 *
 *  Created on: Feb 9, 2019
 *      Author: chungnguyen
 */

#ifndef SNAKE_SNAKE_H_
#define SNAKE_SNAKE_H_
#include "P10.h"

#define BIEN_TREN (8)
#define BIEN_DUOI (31)
#define BIEN_TRAI (0)
#define BIEN_PHAI (31)
#define MAX_DOT (BIEN_DUOI - BIEN_TREN - 1)*(BIEN_PHAI - BIEN_TRAI - 1)

enum
{
	TRAI = 1,
	PHAI,
	LEN,
	XUONG
} huong;

typedef enum
{
	LEVEL1 = 1,
	LEVEL2 = 2,
	LEVEL3 = 3,
	LEVEL4 = 4,
	LEVEL5 = 5,
	LEVEL6 = 6,
	LEVEL7 = 7,
	LEVEL8 = 8,
	LEVEL9 = 9,
	LEVEL10 = 10,
	MAX_NUM_LEVEL
} speed_t;

typedef enum
{
	STOP = 0,
	START = 1,
} start_t;

typedef enum
{
	NOPAUSE = 0,
	PAUSE
} pause_t;

typedef enum
{
	SPACE,
	WALL,
	IS_SNAKE
} properties_t;

typedef struct
{
	point_2D_t dot[MAX_DOT];
	uint16_t soDotRan;
	uint8_t mauDauRan:3;
	uint8_t mauThanRan:3;
	uint8_t huongCu;
	uint8_t huongMoi;
} ran_t;

typedef struct
{
	point_2D_t dot;
	uint8_t mauMoi:3;
} moi_t;

typedef struct
{
	point_2D_t dot;
	uint8_t mauVatCan:3;
	uint8_t tocDo;
	uint8_t soDotVatCan:3;
	uint8_t huong;
} vatCan_t;

typedef struct
{
	bool code[MAX_ROW][MAX_COL];
} map_t;

typedef struct
{
	speed_t tocDo;
	uint16_t diemSo;
	bool start;
	bool pause;
	bool gameOver;
	uint32_t timeOutGameOver;
	uint8_t mauTuong:3;
	uint8_t map;
	uint8_t count;
	uint8_t number;
	bool beginStart;
} thamSo_t;

void SNAKE_initStart(void);
void SNAKE_blinkWhenGameOver(void);
void SNAKE_showGameOver(void);
bool SNAKE_process(void);
speed_t SNAKE_getSpeed(void);
void SNAKE_setSpeed(speed_t v);
uint8_t SNAKE_getMap(void);
void SNAKE_setMap(uint8_t num);

#endif /* SNAKE_SNAKE_H_ */
