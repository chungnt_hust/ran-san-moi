/*
 * timeCheck.c
 *
 *  Created on: Feb 19, 2019
 *      Author: chungnguyen
 */
#include "timeCheck.h"

uint32_t checkElapsedTime(uint32_t new, uint32_t old)
{
	return (new - old);
}

bool checkPassTime(uint32_t new, uint32_t old)
{
	return (checkElapsedTime(new, old) < 10000 ? true : false);
}


