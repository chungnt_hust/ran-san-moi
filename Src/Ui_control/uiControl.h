/*
 * button.h
 *
 *  Created on: Feb 4, 2019
 *      Author: chungnguyen
 */

#ifndef UI_CONTROL_UICONTROL_H_
#define UI_CONTROL_UICONTROL_H_
#include "stm32f3xx_hal.h"
#include <stdbool.h>

enum
{
	UP_PIN = 0,
	DOWN_PIN,
	LEFT_PIN,
	RIGHT_PIN,
	ENTER_PIN,
	MAX_BUTTON_PIN
};

enum
{
	INACTION = 0,
	SETTING,
	GAME_RUNNING,
	GAME_OVER
};

typedef enum
{
	NONE = 0,
	PRESS,
	RELEASE,
} buttonState_t;

typedef enum
{
	BTN_CTRL_NONE = 0,
} btn_control_state_t;
typedef struct
{
	uint8_t timeCheck;
	buttonState_t state;
} button_t;

typedef struct
{
	uint8_t up;
	uint8_t down;
	uint8_t left;
	uint8_t right;
	uint8_t enter;
	void (*func)(void);
} menu_entry_t;

void UI_buttonScan(void);
void UI_process(void);
uint8_t UI_getUiState(void);
void UI_setUiState(uint8_t state);
void UI_ShowHomeMenu(void);
#endif /* UI_CONTROL_UICONTROL_H_ */
