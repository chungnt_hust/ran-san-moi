/*
 * random.h
 *
 *  Created on: Jan 31, 2019
 *      Author: chungnguyen
 */

#ifndef RANDOM_H_
#define RANDOM_H_
#include "stm32f3xx_hal.h"
#include <time.h>
#include <stdlib.h>

void Random_Init(void);
uint16_t Random_Int(uint16_t limit);
float Random_Float(void);
uint8_t Random_inRange(uint8_t a, uint8_t b);
#endif /* RANDOM_H_ */
