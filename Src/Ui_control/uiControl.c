/*
 * button.c
 *
 *  Created on: Feb 4, 2019
 *      Author: chungnguyen
 */
#include "uiControl.h"
#include "snake.h"
#include "P10.h"
#include "timeCheck.h"
#define TIME_OUT 3000 // 3s
static GPIO_TypeDef *gpioPort[] = {GPIOB, GPIOB, GPIOB, GPIOB, GPIOA};
static uint16_t gpioPin[]       = {GPIO_PIN_12, GPIO_PIN_13, GPIO_PIN_14, GPIO_PIN_15, GPIO_PIN_8};
static button_t button[MAX_BUTTON_PIN];
static uint8_t UI_State = INACTION;
static void showHome(void);
static void showSelectMap(void);
static void showSettingLevel(void);
static void showStartSelect(void);
static void setMapUp(void);
static void setMapDown(void);
static void setLevelUp(void);
static void setLevelDown(void);
static void setStart(void);
static const menu_entry_t menu[] =
{
	{0, 0, 0, 1, 0, showHome},          // 0
	{4, 5, 0, 2, 0, showSelectMap},     // 1
	{6, 7, 1, 3, 0, showSettingLevel},  // 2
	{3, 3, 2, 3, 8, showStartSelect},   // 3

	{4, 5, 0, 2, 4, setMapUp},          // 4
	{4, 5, 0, 2, 5, setMapDown},        // 5
	{6, 7, 1, 3, 6, setLevelUp},        // 6
	{6, 7, 1, 3, 7, setLevelDown},      // 7
	{0, 0, 0, 0, 0, setStart},          // 8
};
static uint8_t selectPointer;

extern ran_t ran;
extern thamSo_t thamSo;
extern volatile uint32_t g_SysTime;

static void btnCallback(uint8_t btnNum)
{
	//printf("bt %d callback\r\n", btnNum);
	switch(UI_State)
	{
		case INACTION:
			if(btnNum == UP_PIN)
			{
				selectPointer = menu[selectPointer].up;
			}

			else if(btnNum == DOWN_PIN)
			{
				selectPointer = menu[selectPointer].down;
			}

			else if(btnNum == LEFT_PIN)
			{
				selectPointer = menu[selectPointer].left;
			}

			else if(btnNum == RIGHT_PIN)
			{
				selectPointer = menu[selectPointer].right;
			}

			else if(btnNum == ENTER_PIN)
			{
				selectPointer = menu[selectPointer].enter;
			}
			menu[selectPointer].func();
			break;
		case SETTING:
			break;
		case GAME_RUNNING:
			if(btnNum == UP_PIN)
			{
				thamSo.pause = NOPAUSE;
				if(ran.huongCu != XUONG && ran.huongCu != LEN)
				{
					ran.huongMoi = LEN;
				}
			}

			else if(btnNum == DOWN_PIN)
			{
				thamSo.pause = NOPAUSE;
				if(ran.huongCu != LEN && ran.huongCu != XUONG)
				{
					ran.huongMoi = XUONG;
				}
			}

			else if(btnNum == LEFT_PIN)
			{
				thamSo.pause = NOPAUSE;
				if(ran.huongCu != PHAI && ran.huongCu != TRAI)
				{
					ran.huongMoi = TRAI;
				}
			}

			else if(btnNum == RIGHT_PIN)
			{
				thamSo.pause = NOPAUSE;
				if(ran.huongCu != TRAI && ran.huongCu != PHAI)
				{
					ran.huongMoi = PHAI;
				}
			}

			else if(btnNum == ENTER_PIN)
			{
				thamSo.pause = !thamSo.pause;
			}
			break;
		case GAME_OVER:
			break;
	}
}

void UI_buttonScan(void)
{
	for(uint8_t btNum = 0; btNum < MAX_BUTTON_PIN; btNum++)
	{
		switch(button[btNum].state)
		{
			case NONE:
				if(HAL_GPIO_ReadPin(gpioPort[btNum], gpioPin[btNum]) == 0)
				{
					button[btNum].timeCheck++;
					if(button[btNum].timeCheck > 3)
					{
						button[btNum].timeCheck = 0;
						if(HAL_GPIO_ReadPin(gpioPort[btNum], gpioPin[btNum]) == 0) button[btNum].state = PRESS;
						else button[btNum].state = NONE;
					}
				}
				break;
			case PRESS:
				btnCallback(btNum);
				button[btNum].state = RELEASE;
				break;
			case RELEASE:
				if(HAL_GPIO_ReadPin(gpioPort[btNum], gpioPin[btNum]) == 1) button[btNum].state = NONE;
				break;
		}
	}
}
// hàm chính xử lý giao diện P10
void UI_process(void) // call 50ms
{
	static uint8_t count = 0;
	static uint8_t timeBlink = 0;
	switch(UI_State)
	{
		case INACTION:
			// xử lý cài đặt game: tốc độ, màu sắc..
			break;
		case GAME_RUNNING:
			if(SNAKE_process())
			{
				UI_State = GAME_OVER;
			}
			break;
		case GAME_OVER:
			if(count < 40)
			{
				count++;
				if(++timeBlink == 4) // 200ms
				{
					timeBlink = 0;
					SNAKE_blinkWhenGameOver();
				}
			}
			else if(count == 40)
			{
				count++;
				SNAKE_showGameOver();
				thamSo.timeOutGameOver = g_SysTime + TIME_OUT;
			}
			else if(checkPassTime(g_SysTime, thamSo.timeOutGameOver))
			{
				count = 0;
				menu[selectPointer].func();
				UI_State = INACTION;
			}
			break;
	}
}

uint8_t UI_getUiState(void)
{
	return UI_State;
}

void UI_setUiState(uint8_t state)
{
	UI_State = state;
}

void UI_ShowHomeMenu(void)
{
	P10_clearScreen();
	P10_sendString(13, 8, (uint8_t*)&"SNAKE", FONT54, Red);
	P10_drawframe(11, 6, 11+8, 6+22, Green);
	P10_sendString(12, 0, (uint8_t*)&">", FONT76, Magenta);
}

static void showHome(void)
{
	UI_ShowHomeMenu();
}

static void showSettingLevel(void)
{
	P10_clearScreen();
	P10_sendString(13, 8, (uint8_t*)&"LEVEL", FONT54, Red);
	P10_drawframe(11, 6, 19, 28, Green);
	P10_sendString(12, 0, (uint8_t*)&">", FONT76, Magenta);
	P10_drawframe(22, 6, 25, 17, Green);
	speed_t cnt = SNAKE_getSpeed();
	for(uint8_t i = 0; i < cnt; i++)
	{
		P10_setColorType1At(23, 7+i, Blue);
		P10_setColorType1At(24, 7+i, Blue);
	}
}

static void showSelectMap(void)
{
	P10_clearScreen();
	P10_sendString(13, 8, (uint8_t*)&"MAP: ", FONT54, Red);
	P10_drawframe(11, 6, 19, 28, Green);
	P10_sendString(12, 0, (uint8_t*)&">", FONT76, Magenta);
	P10_sendNumber(13, 23, SNAKE_getMap(), FONT54, Blue);
}

static void showStartSelect(void)
{
	P10_clearScreen();
	P10_sendString(13, 8, (uint8_t*)&"START", FONT54, Red);
	P10_drawframe(11, 6, 19, 28, Green);
	P10_sendString(12, 0, (uint8_t*)&">", FONT76, Magenta);
}


static void setMapUp(void)
{
	uint8_t mapId = SNAKE_getMap();
	if(++mapId < 10)
	{
		SNAKE_setMap(mapId);
		P10_sendNumber(13, 23, mapId, FONT54, Blue);
	}
}

static void setMapDown(void)
{
	uint8_t mapId = SNAKE_getMap();
	if(--mapId > 0)
	{
		SNAKE_setMap(mapId);
		P10_sendNumber(13, 23, mapId, FONT54, Blue);
	}
}

static void setLevelUp(void)
{
	speed_t cnt = SNAKE_getSpeed();
	if(cnt < 10) SNAKE_setSpeed(++cnt);
	for(uint8_t i = 0; i < 10; i++)
	{
		if(i < cnt)
		{
			P10_setColorType1At(23, 7+i, Blue);
			P10_setColorType1At(24, 7+i, Blue);
		}
		else
		{
			P10_setColorType1At(23, 7+i, NoColor);
			P10_setColorType1At(24, 7+i, NoColor);
		}
	}
}

static void setLevelDown(void)
{
	speed_t cnt = SNAKE_getSpeed();
	if(cnt > 1) SNAKE_setSpeed(--cnt);
	for(uint8_t i = 0; i < 10; i++)
	{
		if(i < cnt)
		{
			P10_setColorType1At(23, 7+i, Blue);
			P10_setColorType1At(24, 7+i, Blue);
		}
		else
		{
			P10_setColorType1At(23, 7+i, NoColor);
			P10_setColorType1At(24, 7+i, NoColor);
		}
	}
}

static void setStart(void)
{
	UI_State = GAME_RUNNING;
	thamSo.start = START;
	SNAKE_initStart();
	selectPointer = 3; // return show start when gamover
}
