/*
 * P10.c
 *
 *  Created on: Jan 24, 2019
 *      Author: chungnguyen
 */
#include "P10.h"
#include "random.h"
#include "../Ui_control/uiControl.h"
#include "font.h"
#include <string.h>

static GPIO_TypeDef *gpioPort[]        = {GPIOA, GPIOA, GPIOA, GPIOA, GPIOA, GPIOA, GPIOB, GPIOB, GPIOB, GPIOA, GPIOA, GPIOB};
static const uint32_t valueGpioSet[]   = {GPIO_PIN_0, GPIO_PIN_1, GPIO_PIN_2, GPIO_PIN_3, GPIO_PIN_4, GPIO_PIN_5, GPIO_PIN_0, GPIO_PIN_1, GPIO_PIN_2, GPIO_PIN_6, GPIO_PIN_7, GPIO_PIN_10};
static const uint32_t valueGpioReSet[] = {GPIO_PIN_0<<16, GPIO_PIN_1<<16, GPIO_PIN_2<<16, GPIO_PIN_3<<16, GPIO_PIN_4<<16, GPIO_PIN_5<<16, GPIO_PIN_0<<16, GPIO_PIN_1<<16, GPIO_PIN_2<<16, GPIO_PIN_6<<16, GPIO_PIN_7<<16, GPIO_PIN_10<<16};


static pixel_t Panel[MAX_ROW][MAX_COL]; // dữ liệu = true => đèn sáng
static const pixel_t colorCode[MAX_COLOR] =
{
	{0, 0, 0},
	{1, 0, 0},
	{0, 1, 0},
	{0, 0, 1},
	{1, 1, 0},
	{0, 1, 1},
	{1, 0, 1},
	{1, 1, 1}
};
static point_2D_t P10;
static uint8_t *strPointer;
static uint8_t strLen;
static scrollType_t scrollEnable = SCROLL_DISABLE;
extern volatile uint32_t g_SysTime;
extern font76_t font76[];
extern font54_t font54[];
static inline void switchRowP10(uint8_t row)
{
	(row & 0x1) ? (gpioPort[A_PIN]->BSRR = (uint32_t)valueGpioSet[A_PIN]):(gpioPort[A_PIN]->BSRR = (uint32_t)valueGpioReSet[A_PIN]);
	(row & 0x2) ? (gpioPort[B_PIN]->BSRR = (uint32_t)valueGpioSet[B_PIN]):(gpioPort[B_PIN]->BSRR = (uint32_t)valueGpioReSet[B_PIN]);
	(row & 0x4) ? (gpioPort[C_PIN]->BSRR = (uint32_t)valueGpioSet[C_PIN]):(gpioPort[C_PIN]->BSRR = (uint32_t)valueGpioReSet[C_PIN]);
//	gpioPort[A_PIN]->ODR = ((gpioPort[A_PIN]->ODR)&0xf8) | row;
}

static inline void enablePanel(void)
{
	gpioPort[OE_PIN]->BSRR = valueGpioReSet[OE_PIN];
}

static inline void disablePanel(void)
{
	gpioPort[OE_PIN]->BSRR = valueGpioSet[OE_PIN];
}

static inline void generateClock(void)
{
	gpioPort[CLK_PIN]->BSRR = valueGpioReSet[CLK_PIN];
	gpioPort[CLK_PIN]->BSRR = valueGpioSet[CLK_PIN];
}

static inline void shiftData(uint8_t row)
{
	for(uint8_t i = 0; i < 2; i++)
	{
		uint8_t rowTem1 = row + 16*i;
		uint8_t rowTem2 = row + 8 + 16*i;
		for(uint8_t col = 0; col < MAX_COL; col++)
		{
			Panel[rowTem1][col].r ? (gpioPort[R1_PIN]->BSRR = valueGpioSet[R1_PIN]) : (gpioPort[R1_PIN]->BSRR = valueGpioReSet[R1_PIN]);
			Panel[rowTem1][col].g ? (gpioPort[G1_PIN]->BSRR = valueGpioSet[G1_PIN]) : (gpioPort[G1_PIN]->BSRR = valueGpioReSet[G1_PIN]);
			Panel[rowTem1][col].b ? (gpioPort[B1_PIN]->BSRR = valueGpioSet[B1_PIN]) : (gpioPort[B1_PIN]->BSRR = valueGpioReSet[B1_PIN]);
			Panel[rowTem2][col].r ? (gpioPort[R2_PIN]->BSRR = valueGpioSet[R2_PIN]) : (gpioPort[R2_PIN]->BSRR = valueGpioReSet[R2_PIN]);
			Panel[rowTem2][col].g ? (gpioPort[G2_PIN]->BSRR = valueGpioSet[G2_PIN]) : (gpioPort[G2_PIN]->BSRR = valueGpioReSet[G2_PIN]);
			Panel[rowTem2][col].b ? (gpioPort[B2_PIN]->BSRR = valueGpioSet[B2_PIN]) : (gpioPort[B2_PIN]->BSRR = valueGpioReSet[B2_PIN]);
			generateClock();
		}
	}
}

static inline void latchData(void)
{
	gpioPort[LAT_PIN]->BSRR = (uint32_t)valueGpioReSet[LAT_PIN];
	gpioPort[LAT_PIN]->BSRR = (uint32_t)valueGpioSet[LAT_PIN];
}

static inline void P10_IRQHandle(void)
{
	static uint8_t row;
	disablePanel();
	shiftData(row);
	latchData();
	switchRowP10(row);
	enablePanel();

	row++;
	if(row == MAX_ROW_SCAN) row = 0;
}

/* system tick interrupt callback 2ms => 62fps */
void HAL_SYSTICK_Callback(void)
{
	P10_IRQHandle();
}

/* 0 <= row <= 15; 0 <= col <= 32 */
static void P10_position(uint8_t row, uint8_t col)
{
	P10.x = col;
	P10.y = row;
}

/* r, g, b = 0/1 */
static void P10_color(bool r, bool g, bool b)
{
	Panel[P10.y][P10.x].r = r;
	Panel[P10.y][P10.x].g = g;
	Panel[P10.y][P10.x].b = b;
}

static void P10_setColor(enumColor_t color)
{
	Panel[P10.y][P10.x].r = colorCode[color].r;
	Panel[P10.y][P10.x].g = colorCode[color].g;
	Panel[P10.y][P10.x].b = colorCode[color].b;
}

void P10_setColorType1At(uint8_t row, uint8_t col, enumColor_t color)
{
	P10_position(row, col);
	P10_setColor(color);
}

void P10_setColorType2At(uint8_t row, uint8_t col, bool r, bool g, bool b)
{
	P10_position(row, col);
	P10_color(r, g, b);
}

void P10_clearScreen(void)
{
	for(uint8_t row = 0; row < MAX_ROW; row++)
	{
		for(uint8_t col = 0; col < MAX_COL; col++)
		{
			Panel[row][col].r = Panel[row][col].g = Panel[row][col].b = 0;
		}
	}
}

void P10_clearArea(uint8_t row_upperLeftCorner, uint8_t col_upperLeftCorner, uint8_t row_underRightCorner, uint8_t col_underRightCorner)
{
	for(uint8_t row = row_upperLeftCorner; row <= row_underRightCorner; row++)
	{
		for(uint8_t col = col_upperLeftCorner; col <= col_underRightCorner; col++)
		{
			Panel[row][col].r = Panel[row][col].g = Panel[row][col].b = 0;
		}
	}
}

void P10_sendString(uint8_t row, uint8_t col, uint8_t *str, formatFont_t fontFormat, enumColor_t color)
{
	uint8_t len = strlen((const char*)str);
	for(uint8_t numChar = 0; numChar < len; numChar++)
	{
		if(fontFormat == FONT76)
		{
			for(uint8_t x = 0; x < 6; x++)
			{
				for(uint8_t y = 0; y < 7; y++)
				{
					P10_setColorType2At(y + row, x + 6*numChar + col, font76[str[numChar]-32].code[y][x]*colorCode[color].r, font76[str[numChar]-32].code[y][x]*colorCode[color].g, font76[str[numChar]-32].code[y][x]*colorCode[color].b);
				}
			}
		}
		else if(fontFormat == FONT54)
		{
			for(uint8_t x = 0; x < 4; x++)
			{
				for(uint8_t y = 0; y < 5; y++)
				{
					P10_setColorType2At(y + row, x + 4*numChar + col, font54[str[numChar]-32].code[y][x]*colorCode[color].r, font54[str[numChar]-32].code[y][x]*colorCode[color].g, font54[str[numChar]-32].code[y][x]*colorCode[color].b);
				}
			}
		}
	}
}

void P10_sendNumber(uint8_t row, uint8_t col, uint8_t number, formatFont_t fontFormat, enumColor_t color)
{
	if(fontFormat == FONT76)
	{
		for(uint8_t x = 0; x < 6; x++)
		{
			for(uint8_t y = 0; y < 7; y++)
			{
				if(y + row == MAX_ROW || x + col == MAX_COL) break;
				P10_setColorType2At(y + row, x + col, font76[number+16].code[y][x]*colorCode[color].r, font76[number+16].code[y][x]*colorCode[color].g, font76[number+16].code[y][x]*colorCode[color].b);
			}
		}
	}
	else if(fontFormat == FONT54)
	{
		for(uint8_t x = 0; x < 4; x++)
		{
			for(uint8_t y = 0; y < 5; y++)
			{
				if(y + row == MAX_ROW || x + col == MAX_COL) break;
				P10_setColorType2At(y + row, x + col, font54[number+16].code[y][x]*colorCode[color].r, font54[number+16].code[y][x]*colorCode[color].g, font54[number+16].code[y][x]*colorCode[color].b);
			}
		}
	}
}

void P10_drawframe(uint8_t row_upperLeftCorner, uint8_t col_upperLeftCorner, uint8_t row_underRightCorner, uint8_t col_underRightCorner, enumColor_t color)
{
	for(uint8_t x = row_upperLeftCorner; x <= row_underRightCorner; x++)
	{
		P10_setColorType1At(x, col_upperLeftCorner, color);
		P10_setColorType1At(x, col_underRightCorner, color);
	}

	for(uint8_t y = col_upperLeftCorner; y <= col_underRightCorner; y++)
	{
		P10_setColorType1At(row_upperLeftCorner, y, color);
		P10_setColorType1At(row_underRightCorner, y, color);
	}
}

// Các hàm để dịch chuyển chữ ở trạng thái cài đặt, độ dài chuỗi chỉ được 5 ký tự
void P10_scroll(uint8_t *str, scrollType_t direct)
{
	strPointer = str;
	strLen = strlen((const char*)str);
	scrollEnable = direct;
}

void P10_scrollProcess(void)
{
	static uint8_t bitPos = 0;
	if(scrollEnable == SCROLL_LEFT)
	{
		if(strLen > 0)
		{
			if(bitPos < 4)
			{
				for(uint8_t row = 13; row <= 17; row++)
				{
					for(uint8_t col = 8; col <= 26; col++)
					{
						Panel[row][col].r = Panel[row][col+1].r;
					}
					Panel[row][27].r = font54[strPointer[5-strLen]-32].code[row-13][bitPos];
				}
				if(++bitPos == 4)
				{
					bitPos = 0;
					if(--strLen == 0) scrollEnable = SCROLL_DISABLE;
				}
			}
		}
	}
	else
	if(scrollEnable == SCROLL_RIGHT)
	{
		if(strLen > 0)
		{
			if(bitPos < 4)
			{
				for(uint8_t row = 13; row <= 17; row++)
				{
					for(uint8_t col = 27; col >= 9; col--)
					{
						Panel[row][col].r = Panel[row][col-1].r;
					}
					Panel[row][8].r = font54[strPointer[strLen-1]-32].code[row-13][3-bitPos];
				}
				if(++bitPos == 4)
				{
					bitPos = 0;
					if(--strLen == 0) scrollEnable = SCROLL_DISABLE;
				}
			}
		}
	}
//	else
//	if(scrollEnable == SCROLL_UP)
//	{
//		if(bitPos < 5)
//		{
//			for(uint8_t col = 8; col <= 27; col++)
//			{
//				for(uint8_t row = 13; row <= 16; row++)
//				{
//					Panel[row][col].r = Panel[row+1][col].r;
//				}
//				Panel[17][col].r = font54[strPointer[(col-8)%5]-32].code[bitPos][(col-8)%4];
//			}
//			if(++bitPos == 5)
//			{
//				bitPos = 0;
//				scrollEnable = SCROLL_DISABLE;
//			}
//		}
//	}
}

enumColor_t P10_getColorAt(uint8_t row, uint8_t col)
{
	if(Panel[row][col].r == 0 && Panel[row][col].g == 0 && Panel[row][col].b == 0) return NoColor;
	else if(Panel[row][col].r == 1 && Panel[row][col].g == 0 && Panel[row][col].b == 0) return Red;
	else if(Panel[row][col].r == 0 && Panel[row][col].g == 1 && Panel[row][col].b == 0) return Green;
	else if(Panel[row][col].r == 0 && Panel[row][col].g == 0 && Panel[row][col].b == 1) return Blue;
	else if(Panel[row][col].r == 1 && Panel[row][col].g == 1 && Panel[row][col].b == 0) return Yellow;
	else if(Panel[row][col].r == 0 && Panel[row][col].g == 1 && Panel[row][col].b == 1) return Cyan;
	else if(Panel[row][col].r == 1 && Panel[row][col].g == 0 && Panel[row][col].b == 1) return Magenta;
	else if(Panel[row][col].r == 1 && Panel[row][col].g == 1 && Panel[row][col].b == 1) return White;
	else return NoColor;
}
