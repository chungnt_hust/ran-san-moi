/*
 * timeCheck.h
 *
 *  Created on: Feb 19, 2019
 *      Author: chungnguyen
 */

#ifndef TIMECHECK_TIMECHECK_H_
#define TIMECHECK_TIMECHECK_H_
#include "stm32f3xx_hal.h"
#include <stdbool.h>

uint32_t checkElapsedTime(uint32_t new, uint32_t old);
bool checkPassTime(uint32_t new, uint32_t old);


#endif /* TIMECHECK_TIMECHECK_H_ */
