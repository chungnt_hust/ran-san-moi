/*
 * snake.c
 *
 *  Created on: Feb 9, 2019
 *      Author: chungnguyen
 */
#include "snake.h"
#include "random.h"
#include "stdio.h"
/* !NOTE: màu của đầu (thân) rắn, mồi và tường là phải khác nhau để dễ xử lý */
static moi_t moi;
static vatCan_t vcMap7[1], vcMap8[2], vcMap9[4];
ran_t ran;
thamSo_t thamSo =
{
	11-LEVEL10, // tốc độ di chuyển từ 50ms -> 500ms
	0,
	STOP,
	NOPAUSE,
	true,
	false,
	Blue,
	0,
	0,
	3,
	false
};
extern const map_t map[];
static void SNAKE_khoiTaoMap(void)
{
	for(uint8_t x = 0 ; x < MAX_COL; x++)
	{
		for(uint8_t y = 0; y < MAX_ROW; y++)
		{
			if(map[thamSo.map].code[y][x] == WALL)
			{
				P10_setColorType1At(y, x, thamSo.mauTuong);
			}
		}
	}
	printf("khoi tao map ok\n");
}

static void SNAKE_khoiTaoVatCan(uint8_t mapId)
{
	if(mapId == 6) // map7
	{
		vcMap7[0].dot.x = 15;
		vcMap7[0].dot.y = 9;
		vcMap7[0].mauVatCan = thamSo.mauTuong;
		vcMap7[0].tocDo = 4; // 200ms
		vcMap7[0].soDotVatCan = 1;
		vcMap7[0].huong = XUONG;
		for(uint8_t i = 0; i < vcMap7[0].soDotVatCan; i++) P10_setColorType1At(vcMap7[i].dot.y, vcMap7[i].dot.x, vcMap7[i].mauVatCan);
	}
	else if(mapId == 7) // map8
	{
		vcMap8[0].dot.x = 4;
		vcMap8[0].dot.y = 9;
		vcMap8[0].mauVatCan = thamSo.mauTuong;
		vcMap8[0].tocDo = 4; // 200ms
		vcMap8[0].huong = XUONG;
		vcMap8[1].dot.x = 27;
		vcMap8[1].dot.y = 30;
		vcMap8[1].mauVatCan = thamSo.mauTuong;
		vcMap8[1].tocDo = 4; // 200ms
		vcMap8[1].huong = LEN;
		vcMap8[0].soDotVatCan = 2;
		for(uint8_t i = 0; i < vcMap8[0].soDotVatCan; i++) P10_setColorType1At(vcMap8[i].dot.y, vcMap8[i].dot.x, vcMap8[i].mauVatCan);
	}
	else if(mapId == 8) // map9
	{
		vcMap9[0].dot.x = 4;
		vcMap9[0].dot.y = 9;
		vcMap9[0].mauVatCan = thamSo.mauTuong;
		vcMap9[0].tocDo = 4; // 200ms
		vcMap9[0].huong = XUONG;
		vcMap9[1].dot.x = 27;
		vcMap9[1].dot.y = 30;
		vcMap9[1].mauVatCan = thamSo.mauTuong;
		vcMap9[1].tocDo = 4; // 200ms
		vcMap9[1].huong = LEN;
		vcMap9[2].dot.x = 1;
		vcMap9[2].dot.y = 26;
		vcMap9[2].mauVatCan = thamSo.mauTuong;
		vcMap9[2].tocDo = 4; // 200ms
		vcMap9[2].huong = PHAI;
		vcMap9[3].dot.x = 30;
		vcMap9[3].dot.y = 12;
		vcMap9[3].mauVatCan = thamSo.mauTuong;
		vcMap9[3].tocDo = 4; // 200ms
		vcMap9[3].huong = TRAI;
		vcMap9[0].soDotVatCan = 4;
		for(uint8_t i = 0; i < vcMap9[0].soDotVatCan; i++) P10_setColorType1At(vcMap9[i].dot.y, vcMap9[i].dot.x, vcMap9[i].mauVatCan);
	}
}

static void SNAKE_vatCanDiChuyen(uint8_t mapId)
{
	static uint8_t count = 0;
	vatCan_t *ptr = NULL;
	if(mapId == 6) ptr = &vcMap7[0];
	else if(mapId == 7) ptr = &vcMap8[0];
	else if(mapId == 8) ptr = &vcMap9[0];
	if(ptr != NULL)
	{
		if(++count == ptr->tocDo)
		{
			count = 0;
			for(uint8_t vc = 0; vc < ptr->soDotVatCan; vc++)
			{
				P10_setColorType1At((ptr+vc)->dot.y, (ptr+vc)->dot.x, NoColor);
				switch((ptr+vc)->huong)
				{
					case XUONG:
						if(++(ptr+vc)->dot.y >= 30) {(ptr+vc)->dot.y = 30; (ptr+vc)->huong = LEN;}
						break;
					case LEN:
						if(--(ptr+vc)->dot.y <= 9)  {(ptr+vc)->dot.y = 9; (ptr+vc)->huong = XUONG;}
						break;
					case PHAI:
						if(++(ptr+vc)->dot.x >= 30) {(ptr+vc)->dot.x = 30; (ptr+vc)->huong = TRAI;}
						break;
					case TRAI:
						if(--(ptr+vc)->dot.x <= 1)  {(ptr+vc)->dot.x = 1; (ptr+vc)->huong = PHAI;}
						break;
				}
				P10_setColorType1At((ptr+vc)->dot.y, (ptr+vc)->dot.x, (ptr+vc)->mauVatCan);
			}
		}
	}
}

static void SNAKE_khoiTaoRan(void)
{
	// khởi tạo 3 đốt rắn và thuộc tính từng đốt
	ran.huongCu = ran.huongMoi = PHAI;
	ran.soDotRan = 3;
	ran.dot[2].x = BIEN_TRAI+5;
	ran.dot[1].x = BIEN_TRAI+6;
	ran.dot[0].x = BIEN_TRAI+7;
	ran.dot[0].y = ran.dot[1].y = ran.dot[2].y = BIEN_TREN+5;
	ran.mauDauRan = Magenta;
	ran.mauThanRan = Yellow;
	// khởi tạo mồi
	moi.mauMoi = Green;
	// tô màu cho rắn
	P10_setColorType1At(ran.dot[0].y, ran.dot[0].x, ran.mauDauRan);
	for(uint8_t i = 1; i < ran.soDotRan; i++) P10_setColorType1At(ran.dot[i].y, ran.dot[i].x, ran.mauThanRan);
	// khởi tạo tham số
	thamSo.diemSo = 0;
	thamSo.gameOver = false;
	thamSo.count = 0;
	thamSo.number = 3;
	thamSo.beginStart = false;
	printf("khoi tao ran ok\n");
}

static void SNAKE_diChuyenRan(void)
{
	// xóa tọa độ cuối cũ
	point_2D_t dotCuoiCu = ran.dot[ran.soDotRan-1];
	P10_setColorType1At(dotCuoiCu.y, dotCuoiCu.x, NoColor);

	// di chuyển rắn theo hướng của đầu rắn
	for(int16_t i = ran.soDotRan - 1; i >= 1; i--)
	{
		ran.dot[i] = ran.dot[i - 1];
	}
	switch(ran.huongMoi)
	{
		case TRAI:
			ran.dot[0].x--;
			break;
		case PHAI:
			ran.dot[0].x++;
			break;
		case LEN:
			ran.dot[0].y--;
			break;
		case XUONG:
			ran.dot[0].y++;
			break;
	}
	// cập nhật hướng đang di chuyển
	ran.huongCu = ran.huongMoi;
	// set màu đầu và thân rắn
	P10_setColorType1At(ran.dot[0].y, ran.dot[0].x, ran.mauDauRan);
	for(uint8_t i = 1; i < ran.soDotRan; i++) P10_setColorType1At(ran.dot[i].y, ran.dot[i].x, ran.mauThanRan);
}

static bool SNAKE_kiemTraThua(uint8_t mapId)
{
	bool lose = false;
	// kiểm tra chạm tường
	if(map[thamSo.map].code[ran.dot[0].y][ran.dot[0].x]) lose = true;
	// kiểm tra chạm vào thân
	for(uint8_t i = 1; i < ran.soDotRan; i++)
	{
		if(ran.dot[i].x == ran.dot[0].x && ran.dot[i].y == ran.dot[0].y)
		{
			lose = true;
			break;
		}
	}
	// kiểm tra chướng ngại vật di chuyển đâm vào rắn
	vatCan_t *ptr = NULL;
	if(mapId == 6) ptr = &vcMap7[0];
	else if(mapId == 7) ptr = &vcMap8[0];
	else if(mapId == 8) ptr = &vcMap9[0];
	if(ptr != NULL)
	{
		for(uint8_t i = 0; i < ran.soDotRan; i++)
		{
			for(uint8_t vc = 0; vc < ptr->soDotVatCan; vc++)
			{
				if(ran.dot[i].x == (ptr+vc)->dot.x && ran.dot[i].y == (ptr+vc)->dot.y)
				{
					lose = true;
					break;
				}
			}
			if(lose) break;
		}
	}
	return lose;
}

static void SNAKE_hienThiMoi(void)
{
	bool check = false;
	uint16_t count = 0;
	uint16_t maxPoint = MAX_DOT - ran.soDotRan;
	do
	{
		count++;
		moi.dot.x = Random_inRange(BIEN_TRAI + 1, BIEN_PHAI - 1);
		moi.dot.y = Random_inRange(BIEN_TREN + 1, BIEN_DUOI - 1);
		enumColor_t color = P10_getColorAt(moi.dot.y, moi.dot.x);
		if(color == ran.mauDauRan || color == ran.mauThanRan || color == thamSo.mauTuong) check = true;
		else
		{
			P10_setColorType1At(moi.dot.y, moi.dot.x, moi.mauMoi);
			check = false;
		}
	} while(check && count < maxPoint);
}

static bool SNAKE_kiemTraDaAnMoi(void)
{
	if(ran.dot[0].x == moi.dot.x && ran.dot[0].y == moi.dot.y) return true;
	else return false;
}

static void SNAKE_nhayMoi(void)
{
	static bool lat = true;
	if(lat)
	{
		P10_setColorType1At(moi.dot.y, moi.dot.x, moi.mauMoi);
		lat = false;
	}
	else
	{
		P10_setColorType1At(moi.dot.y, moi.dot.x, NoColor);
		lat = true;
	}
}

static void SNAKE_them1Dot(void)
{
	ran.soDotRan++;
	ran.dot[ran.soDotRan - 1] = ran.dot[ran.soDotRan - 2];
}

static void SNAKE_hienThiDiem(uint16_t diem)
{
	uint8_t tram, chuc, donvi;
	donvi = diem%10;
	diem /= 10;
	chuc = diem%10;
	diem /= 10;
	tram = diem%10;
	P10_sendNumber(2, 21, tram, FONT54, Magenta);
	P10_sendNumber(2, 25, chuc, FONT54, Magenta);
	P10_sendNumber(2, 29, donvi, FONT54, Magenta);
}

void SNAKE_blinkWhenGameOver(void)
{
	static bool lat = false;
	if(lat == false)
	{
		for(uint8_t i = 1; i < ran.soDotRan; i++) P10_setColorType1At(ran.dot[i].y, ran.dot[i].x, NoColor);
		lat = true;
	}
	else
	{
		for(uint8_t i = 1; i < ran.soDotRan; i++) P10_setColorType1At(ran.dot[i].y, ran.dot[i].x, ran.mauThanRan);
		lat = false;
	}
}

void SNAKE_showGameOver(void)
{
	P10_clearArea(9, 1, 30, 30);
	P10_sendString(15, 8, (uint8_t*)&"GAME", FONT54, Magenta);
	P10_sendString(21, 8, (uint8_t*)&"OVER", FONT54, Magenta);
	printf("game over\n");
}

void SNAKE_initStart(void)
{
	P10_clearScreen();
	Random_Init();
	SNAKE_khoiTaoMap();
	SNAKE_khoiTaoVatCan(thamSo.map);
	SNAKE_khoiTaoRan();
	SNAKE_hienThiMoi();
	P10_sendString(2, 1, (uint8_t*)&"SCORE", FONT54, Red);
	P10_sendNumber(2, 29, thamSo.number, FONT54, Magenta-(enumColor_t)thamSo.number);
}

bool SNAKE_process(void) // call 50ms
{

	if(!thamSo.beginStart)
	{
		if(++thamSo.count == 20)
		{
			thamSo.count = 0;
			thamSo.number--;
			P10_sendNumber(2, 29, thamSo.number, FONT54, Magenta-(enumColor_t)thamSo.number);
		}
		if(thamSo.number == 0)
		{
			SNAKE_hienThiDiem(thamSo.diemSo);
			thamSo.beginStart = true;
		}
	}
	else
	{
		if(thamSo.start && !thamSo.pause)
		{
			SNAKE_nhayMoi(); // 50ms
			SNAKE_vatCanDiChuyen(thamSo.map);
			if(SNAKE_kiemTraThua(thamSo.map) == false)
			{
				if(++thamSo.count == thamSo.tocDo)
				{
					thamSo.count = 0;
					SNAKE_diChuyenRan();
					if(SNAKE_kiemTraDaAnMoi())
					{
						SNAKE_hienThiDiem(++thamSo.diemSo);
						SNAKE_them1Dot();
						SNAKE_hienThiMoi();
					}
				}
			}
			else
			{
				thamSo.count = 0;
				thamSo.start = STOP;
				thamSo.gameOver = true;
			}
		}
		if(thamSo.pause) P10_setColorType1At(moi.dot.y, moi.dot.x, moi.mauMoi);
	}
	return thamSo.gameOver;
}


speed_t SNAKE_getSpeed(void)
{
	return (MAX_NUM_LEVEL-thamSo.tocDo);
}

void SNAKE_setSpeed(speed_t v)
{
	thamSo.tocDo = (MAX_NUM_LEVEL-v);
}

bool SNAKE_getGameOverFlag(void)
{
	return thamSo.gameOver;
}

uint8_t SNAKE_getMap(void)
{
	return (thamSo.map + 1);
}

void SNAKE_setMap(uint8_t num)
{
	thamSo.map = (num - 1);
}
