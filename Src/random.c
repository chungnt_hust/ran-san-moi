/*
 * random.c
 *
 *  Created on: Jan 31, 2019
 *      Author: chungnguyen
 */
#include "random.h"
extern volatile uint32_t g_SysTime;
void Random_Init(void)
{
	srand(g_SysTime);
}

uint16_t Random_Int(uint16_t limit)
{
	return rand()%limit;
}

float Random_Float(void)
{
	return ((float)rand()/(float)(RAND_MAX)); // ((float)rand()/(float)(RAND_MAX)) * a => result between 0, 1
}

uint8_t Random_inRange(uint8_t a, uint8_t b)
{
	return (a + rand() % (b - a + 1));
}
