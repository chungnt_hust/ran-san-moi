/*
 * uart_user.c
 *
 *  Created on: Jan 30, 2019
 *      Author: chungnguyen
 */
#include "uart_user.h"
#include "usart.h"
#include <stdio.h>
#include <string.h>
#include "../P10/P10.h"
#include <stdlib.h>

UART_HandleTypeDef *UART = &huart2;

buffer_t bufferRx;

void Uart_Init(void)
{
	HAL_UART_Receive_IT(UART, &bufferRx.Byte, 1);
	printf("Init done!\r\n");
}

void Uart_Process(void)
{
	if(bufferRx.State > 0)
	{
		bufferRx.State--;
		if(bufferRx.State == 0)
		{
			Uart_ProcessNewData();
		}
	}
}

// khung dữ liệu cài thời gian: [xx,xx,xx] với xx lần lượt là giờ, phút, giây
void Uart_ProcessNewData(void)
{
	bufferRx.Data[bufferRx.Index] = 0;
	printf("%s\r\n", bufferRx.Data);

	bufferRx.Index = 0;
	HAL_UART_Receive_IT(UART, &bufferRx.Byte, 1);
}

void Uart_GetNewByte(void)
{
	bufferRx.Data[bufferRx.Index] = bufferRx.Byte;
	bufferRx.Index++;
	bufferRx.State = 3;
	HAL_UART_Receive_IT(UART, &bufferRx.Byte, 1);
}
